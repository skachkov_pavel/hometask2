﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class RabbitMqWrapper : IDisposable
    {
        private EventingBasicConsumer _consumer;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public RabbitMqWrapper(string hostName)
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        public void Send(string message, string queueName)
        {
            DeclareQueueIfNotExists(queueName, _channel);
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "",
                                 routingKey: queueName,
                                 basicProperties: null,
                                 body: body);
        }

        public void ListenQueue(string queueName, EventHandler<BasicDeliverEventArgs> handler)
        {
            DeclareQueueIfNotExists(queueName, _channel);

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += handler;
            _channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: _consumer);

            Console.WriteLine($"Listening {queueName}");
        }

        private static void DeclareQueueIfNotExists(string queueName, IModel channel)
        {
            channel.QueueDeclare(queue: queueName,
                                             durable: false,
                                             exclusive: false,
                                             autoDelete: false,
                                             arguments: null);
        }

        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }
    }
}
