﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Ponger
{
    class Program
    {
        private const string SourceQueueName = "pong_queue";
        private const string TargetQueueName = "ping_queue";
        private const string TargetQueueMessage = "pong";
        private const string RmqHostName = "localhost";

        static void Main(string[] args)
        {
            using var rmqWrapper = new RabbitMqWrapper(RmqHostName);
            rmqWrapper.ListenQueue(SourceQueueName, handler);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();

            void handler(object sender, BasicDeliverEventArgs e)
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($"{DateTime.UtcNow} \t {message}");

                Task.Delay(TimeSpan.FromSeconds(2.5)).Wait();
                rmqWrapper.Send(TargetQueueMessage, TargetQueueName);
            }
        }
    }
}
